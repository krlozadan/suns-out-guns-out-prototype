﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using System.Collections;
using UnityEngine;


public class Enemy : MonoBehaviour
{

    private PlayerController player;
    private Weapon weapon;



    void Awake ()
    {
        player = FindObjectOfType<PlayerController>();
        weapon = GetComponentInChildren<Weapon>();
        StartCoroutine(StateAttacking());
    }

    IEnumerator StateAttacking()
    {
        while(true)
        {
            yield return new WaitForSeconds(3f);
            Vector3 direction = player.transform.position - transform.position;
            direction.Normalize();
            weapon.Shoot(direction);
        }
    }

}