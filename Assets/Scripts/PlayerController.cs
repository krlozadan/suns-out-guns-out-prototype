﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/****************************************************
The playercharacter in this FPS, never actually shoots anything. 
Instead, he gets shot at by turrets constantly, and has to 
dodge them in order to stay alive.
**************************************************** */


public class PlayerController : MonoBehaviour {

		[SerializeField]
        private float _MoveSpeed = 5f;

        [SerializeField]
        private float _JumpSpeed = 12f;

        [SerializeField]
        private Transform _CameraPivot;
		
		[SerializeField]
		private int _Health;

		[SerializeField]
		private float 			_OilBuildup;

		[SerializeField]
		private float			_SlideSpeedMultiplier = 1.5f;

		[SerializeField]
		private float			_OilReplenishRate;

		[SerializeField]
		private float 			_OilUseRate;

		[SerializeField]
		private float			_SlideCooldownTime;

		[SerializeField]
		private OilTrail		_SlimeDrop;

		[SerializeField]
		private OilTrail		_SlimeImage;

		[SerializeField]
		private float			_DropRate = 0.1f;
		private bool			_ReadyToDrop = true;

		private float			_CurrentSpeedMultiplier;
		
        private Rigidbody _RB;
        private Transform _Cam;

        private float _XInput = 0f;
        private float _ZInput = 0f;
        private bool _JumpPressed = false;

		private bool		_Slide;
		private bool		_CanSlide = true;
		
		private SunsOut sunsOut;


    void Start()
	{
		Cursor.lockState=CursorLockMode.Locked;
		_RB = GetComponent<Rigidbody>();
		// The camera pivot contains the camera, not us.
		_Cam = GetComponentInChildren<Camera>().transform;
		sunsOut = GetComponentInChildren<SunsOut>();
	}
	
	private void Update() {
		HandleInput();	
	}

	/******************************************************
	Reads user input, rotates camera accordingly, handles user
	pressing mouse button down for various firing modes
	**************************************************** */
	void HandleInput() {
		HandleKeys();
		HandleMouseMovement();
		HandleReflection();
		HandleSliding();

		ApplyMovementPhysics();
	}

	/*********************************************************
	Player controls camera according to mouse. Perhaps I should refactor
	this, or give the player unit a camera object.
	****************************************************** */
	void HandleMouseMovement(){

		float mouseX = Input.GetAxis("Mouse X");
		float mouseY = Input.GetAxis("Mouse Y");

		//this only works if we're attached to the camera object
		transform.Rotate(0, mouseX, 0, Space.World);		//rotate around self
		transform.Rotate(-mouseY, 0, 0);					//rotate around self
	}

	void HandleReflection() 
	{
		bool sunsOutButtonPressed = Input.GetMouseButtonDown(0);
		bool gunsOutButtonPressed = Input.GetMouseButtonDown(1);
		
		if(sunsOutButtonPressed && !gunsOutButtonPressed)
		{
			sunsOut.Pose();
		}
		else if(gunsOutButtonPressed && !sunsOutButtonPressed)
		{
			Debug.Log("Guns Out!");
		}
	}


	/*********************************************************
	We care about WASD for movement, and ESC for quitting.
	****************************************************** */
	void HandleKeys() {

		_XInput = Input.GetAxis("Horizontal");
        _ZInput = Input.GetAxis("Vertical");
		_Slide = _Slide || Input.GetKey(KeyCode.LeftShift);
		_JumpPressed = Input.GetKeyDown(KeyCode.Space);

	}

	private void ApplyMovementPhysics(){

        // ternary operator
        // if value ? (then) thisVal : (else) thisOtherVal
        Vector3 newVel = new Vector3(_XInput, 0, _ZInput) * _MoveSpeed * _CurrentSpeedMultiplier;
        newVel = transform.TransformVector(newVel);
        newVel.y = _JumpPressed ? _JumpSpeed : _RB.velocity.y;
        _RB.velocity = newVel;

        _JumpPressed = false;
        _Slide = false;
    }

	// Sets the speed multiplier, among other things.
	private void HandleSliding(){
		_CurrentSpeedMultiplier = 1;

		// Check 
		if(_Slide && _CanSlide){
			_CurrentSpeedMultiplier = _SlideSpeedMultiplier;
			_OilBuildup -= _OilUseRate * Time.deltaTime;

			// drop the slime trail
			if(_ReadyToDrop){
				// Instantiate a cube as a test for now.
				Vector3 pos = transform.position + (transform.forward * 1.5f * -1.0f);
				Instantiate(_SlimeDrop, pos, transform.rotation);

				// Instantiate the image of slime, on the ground and with the right rotation.
				// TODO: Make this work with non-flat ground at arbitrary height.
				Vector3 angles = transform.rotation.eulerAngles;
				angles.x = 90f;
				Vector3 trailPos = transform.position;
				trailPos.y = 0.01f;
				Instantiate(_SlimeImage, trailPos, Quaternion.Euler(angles));
				Invoke("DropCooldown", _DropRate);
			}
		}else{
			_OilBuildup += _OilReplenishRate * Time.deltaTime;
		}

		// if we've run out of oil, don't let them slide again until x time has passed.
		if(_OilBuildup <= 0){
			_CanSlide = false;
			Invoke("SlideCooldown", _SlideCooldownTime);
		}

		if(_OilBuildup > 100){
			_OilBuildup = 100;
		}
	}

	private void SlideCooldown(){
		_CanSlide = true;
	}

	private void DropCooldown(){
		_ReadyToDrop = true;
	}

	private void OnGUI()
	{
		string text = "Slide: " + _OilBuildup + " /100";
		GUI.Label(new Rect(10, 10, 100, 20), text);
	}
}
