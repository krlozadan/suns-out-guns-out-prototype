﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class SunsOut : MonoBehaviour
{
    #region Properties

    [SerializeField]
    private float poseTimeframe = 1f;
    [SerializeField]
    private float penaltyRecoveryTime = 2f;
    
    private CapsuleCollider coll;
    private bool canPose = true;
    private float lastPoseTimestamp = -1f;
    private bool bouncedAnyBullet = false;

    #endregion

    #region Unity Functions

    void Awake()
    {
        coll = GetComponent<CapsuleCollider>();
        coll.enabled = false;
    }

    void Update()
    {
        if(lastPoseTimestamp > -1f)
        {
            lastPoseTimestamp += Time.deltaTime;
        }
    }

    #endregion

    #region Bounce Logic

    public void Pose()
    {
        if(canPose == false) return;

        lastPoseTimestamp = Time.time;
        coll.enabled = true;
        canPose = false;
        Invoke("EvaluateRecoveryTime", poseTimeframe);
    }
    
    private void EvaluateRecoveryTime()
    {
        if (bouncedAnyBullet)
        {
            DisablePoseTrigger();
        }
        else
        {
            coll.enabled = false;
            Invoke("DisablePoseTrigger", penaltyRecoveryTime);
        }
    }
    
    private void DisablePoseTrigger()
    {
        canPose = true;
        coll.enabled = false;
        bouncedAnyBullet = false;
    }

    private bool IsWithinPoseTime()
    {
        if(lastPoseTimestamp >= lastPoseTimestamp + poseTimeframe)
        {
            lastPoseTimestamp = -1;
            return false;
        }
        else
        {
            return true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if(bullet != null && IsWithinPoseTime())
        {
            if(bullet.bounced == false)
            {
                bouncedAnyBullet = true;
                bullet.Bounce();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if(enemy != null)
        {
            enemy.GetComponent<Rigidbody>().AddExplosionForce(50f, transform.position, 3f, 3f, ForceMode.Impulse);
        }
    }

    #endregion
}