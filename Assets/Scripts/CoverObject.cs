﻿using UnityEngine;

// right now just exists to bounce the bullet.
public class CoverObject : MonoBehaviour {

    void OnTriggerStay(Collider other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();    
        if(bullet)
        {
            if(bullet.bounced == false)
            {
                bullet.Bounce();
            }
        }
    }

}
