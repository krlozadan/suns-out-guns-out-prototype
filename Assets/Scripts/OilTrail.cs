﻿using UnityEngine;

/********************************************************************
Left behind by the player when they use the oil slide.
******************************************************************** */

[RequireComponent(typeof(LifeSpan))]
public class OilTrail : MonoBehaviour {
    
    // I'm leaving this all blank for now, we'll give it specific properties when it's time to really implement.
    
}
