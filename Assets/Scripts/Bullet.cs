﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody rb;

    public bool bounced = false;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 30f;
        Destroy(gameObject, 10f);
    }

    public void Bounce()
    {
        rb.velocity *= -1;
        bounced = true;
    }

    // Right now used only to make the objects that are "cover" deflect the bullets
    // TODO: Replace with a non-hack.
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.GetComponent<CoverObject>()){
            Bounce();
        }
    }
}