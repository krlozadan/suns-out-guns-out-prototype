﻿/*
*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c)
* Email: krlozadan@gmail.com
* In case we do split screen use XInput
* 
*/

using UnityEngine;

public class InputManager : SingletonBehaviour<InputManager>
{
    
    public float leftStickX { get; private set; }
    public float leftStickY { get; private set; }

    public float rightStickX { get; private set; }
    public float rightStickY { get; private set; }
    public float rightSitckClick { get; private set; }

    public bool JumpButtonPressed { get; private set; } 
    public bool ButtonPressed { get; private set; } 
    public bool XButtonPressed { get; private set; } 
    public bool YButtonPressed { get; private set; } 
    
    protected override void SingletonAwake()
    {
        
    }

    private void Update()
    {
        UpdateInputState();
    }

    private void UpdateInputState()
    {
        
    }
}