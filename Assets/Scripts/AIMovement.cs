﻿using UnityEngine;
using System.Collections;

public enum MoveBehaviour : int {
    MOVE_TO_PLAYER,
    MOVE_TO_NEAREST_COVER,
    MOVE_TO_SPECIFIC_COVER,
    NO_MOVEMENT
}

[RequireComponent(typeof(Rigidbody))]
public class AIMovement : MonoBehaviour {

    private PlayerController player;
    
    private Rigidbody       _RB;

    private CoverObject[]   _CoverSpots;

    [SerializeField]
    private MoveBehaviour   _MovementBehaviour;

    [SerializeField]
    private bool            _HideBehindCover;

    [Header("The distance they'll stop from cover")]
    [SerializeField]
    private float           _DisFromCover = 2.5f;

    [SerializeField]
    private float           _MovementVelocity = 2.5f;

    [SerializeField]
    private CoverObject     _CoverToMoveTo;



    void Awake ()
    {
        player = FindObjectOfType<PlayerController>();

        _RB = GetComponent<Rigidbody>();

        // Gets all the spots of cover in the level.
        _CoverSpots = FindObjectsOfType<CoverObject>();
    }
    
    void Update ()
    {
        if(_MovementBehaviour != MoveBehaviour.NO_MOVEMENT){
            Move();
        }
    }

    /************************************************************************************************
    * either move them directly to the player, or move them directly towards the nearest cover.
    * Or specify which bit of cover for them to move directly towards.
    * We also want the enemy, for now, to move to the spot in cover that is furthest from the player.
    * So they're moving to a spot in a radius around the cover, away from the player.
    ************************************************************************************************ */
    private void Move(){

        // First find the targetted final position, either a player, cover, or back of cover
        // default target is us.
        Vector3 target = transform.position; 

        switch(_MovementBehaviour){
            case MoveBehaviour.NO_MOVEMENT: break;

            case MoveBehaviour.MOVE_TO_SPECIFIC_COVER: target = TargetSpecificCover(); break;
            case MoveBehaviour.MOVE_TO_NEAREST_COVER: target = TargetNearestCover(); break;
            case MoveBehaviour.MOVE_TO_PLAYER: target = TargetPlayer(); break;
        }


        // By now we know what we're moving towards
        Vector3 vel = target - transform.position;
        vel = Vector3.Normalize(vel);
        vel *= _MovementVelocity;

        _RB.velocity = vel;
    }

    private Vector3 TargetPlayer(){
        return player.transform.position;
    }

    private Vector3 TargetSpecificCover(){
        Vector3 goalPos = _CoverToMoveTo.transform.position;
        Vector3 target = transform.position;

        // If moving to the back of the cover
        if(_HideBehindCover){
            goalPos = PlaceTargetBehindCover(goalPos, player.transform.position);
        }

        // If we're not too close, get closer
        if(_DisFromCover < Vector3.Distance(goalPos, transform.position)){
            target = goalPos;
        }

        return target;
    }

    // returns the target.
    private Vector3 TargetNearestCover(){
        Vector3 goalPos;
        Vector3 target = transform.position;

        // find the nearest cover object.
        float disToNearest = Vector3.Distance(_CoverSpots[0].transform.position, transform.position);
        int coverIndex = 0;
        for(int i=0; i<_CoverSpots.Length; i++){
            float temp = Vector3.Distance(_CoverSpots[i].transform.position, transform.position);
            if(temp < disToNearest){
                disToNearest = temp;
                coverIndex = i;
            }
        }

        goalPos = _CoverSpots[coverIndex].transform.position;

        if(_HideBehindCover){
            goalPos = PlaceTargetBehindCover(goalPos, player.transform.position);
            target = goalPos;
        }

        // Only move if we're not already close enough.
        if(_DisFromCover < disToNearest && !_HideBehindCover){
            target = _CoverSpots[coverIndex].transform.position;
        }

        return target;
    }

    // Helper function that takes and returns a target that is behind the cover, in the direction from the player.
    private Vector3 PlaceTargetBehindCover(Vector3 goalPos, Vector3 playerPos){

        Vector3 heading = goalPos - playerPos;
        heading.y = 0;
        heading = heading.normalized;
        // the multiple is just a hack to make the effect more obvious
        goalPos += heading*_DisFromCover*4;

        return goalPos;
    }

}
